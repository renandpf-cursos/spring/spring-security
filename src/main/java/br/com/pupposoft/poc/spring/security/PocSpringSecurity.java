package br.com.pupposoft.poc.spring.security;

import java.util.Locale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocSpringSecurity {
    public static void main(String[] args) throws Exception {
    	Locale.setDefault(new Locale( "pt", "BR" ));
        SpringApplication.run(PocSpringSecurity.class, args);
    }
}
